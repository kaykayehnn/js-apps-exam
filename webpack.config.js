const webpack = require('webpack')
const path = require('path')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

require('dotenv').config()
/*
require('webpack')(env => {
  const srcPath = path.resolve(__dirname, 'src')

  return {
    mode: 'development',
    entry: path.resolve(srcPath, 'index.js'),
    output: {
      path: path.resolve(__dirname, env ? env.outDir : 'dist'),
      filename: '[name].bundle.js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        {
          test: /\.css$/,
          use: [
            'style-loader',
            'css-loader'
          ]
        },
        {
          test: /\.handlebars$/,
          loader: 'handlebars-loader'
        }
      ]
    },
    resolve: {
      alias: {
        Models: path.resolve(srcPath, 'models'),
        Views: path.resolve(srcPath, 'views'),
        Controllers: path.resolve(srcPath, 'controllers'),
        Config: path.resolve(srcPath, 'config'),
        Utilities: path.resolve(srcPath, 'utilities')
      }
    },
    devServer: {
      contentBase: path.resolve(__dirname, 'dist'),
      open: true
    },
    plugins: [
      new CleanWebpackPlugin(['dist', 'build']),
      new HTMLWebpackPlugin({
        title: 'PointOfSale SPA',
        template: path.resolve(srcPath, 'template.html')
      }),
      new webpack.DefinePlugin({
        APP_KEY: JSON.stringify(process.env.APP_KEY),
        APP_SECRET: JSON.stringify(process.env.APP_SECRET)
      })
    ]
  }
})
*/

module.exports = env => {
  const srcPath = path.resolve(__dirname, 'src')

  return {
    mode: 'development',
    entry: path.resolve(srcPath, 'index.js'),
    output: {
      path: path.resolve(__dirname, env ? env.outDir : 'dist'),
      filename: '[name].bundle.js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        {
          test: /\.css$/,
          use: [
            'style-loader',
            'css-loader'
          ]
        },
        {
          test: /\.handlebars$/,
          loader: 'handlebars-loader'
        }
      ]
    },
    resolve: {
      alias: {
        Models: path.resolve(srcPath, 'models'),
        Views: path.resolve(srcPath, 'views'),
        Controllers: path.resolve(srcPath, 'controllers'),
        Config: path.resolve(srcPath, 'config'),
        Utilities: path.resolve(srcPath, 'utilities')
      }
    },
    devServer: {
      contentBase: path.resolve(__dirname, 'dist'),
      open: true
    },
    plugins: [
      new CleanWebpackPlugin(['dist', 'build']),
      new HTMLWebpackPlugin({
        title: 'PointOfSale SPA',
        template: path.resolve(srcPath, 'template.html')
      }),
      new webpack.DefinePlugin({
        APP_KEY: JSON.stringify(process.env.APP_KEY),
        APP_SECRET: JSON.stringify(process.env.APP_SECRET)
      })
    ]
  }
}
