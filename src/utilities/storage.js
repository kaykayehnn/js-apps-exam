const storage = localStorage

export function get (key) {
  return storage.getItem(key)
}

export function set (key, val) {
  let valSerialized = typeof val === 'string' ? val : JSON.stringify(val)
  storage.setItem(key, valSerialized)
}
export function clear () {
  storage.clear()
}
export function saveSession (data) {
  set('username', data.username)
  set('id', data._id)
  set('authtoken', data._kmd.authtoken)
}
