import axios from 'axios'
import * as storage from './storage'
import {toggleLoading} from '../config/notifications'

const base64 = window.btoa(`${APP_KEY}:${APP_SECRET}`)

const instance = axios.create({
  baseURL: 'https://baas.kinvey.com/',
  transformRequest: [(data, headers) => {
    let auth = storage.get('authtoken')
    headers['Authorization'] = auth ? `Kinvey ${auth}` : `Basic ${base64}`
    toggleLoading()
    return JSON.stringify(data)
  }],
  transformResponse: [(data) => {
    toggleLoading()
    return JSON.parse(data || '""')
  }]
})

instance.defaults.headers['Content-Type'] = 'application/json'

export default instance
