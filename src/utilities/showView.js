export default function showView (html) {
  let container = document.getElementById('container')
  let section = document.querySelector('section')
  if (section) section.remove()

  let newHtml = container.innerHTML.replace('</header>', '</header>' + html)
  container.innerHTML = newHtml
}
