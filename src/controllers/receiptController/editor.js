import * as Entry from 'Models/Entry'
import * as Receipt from 'Models/Receipt'
import editorTemplate from 'Views/editor.handlebars'

import showView from 'Utilities/showView'
import * as storage from 'Utilities/storage'
import { showError, showInfo } from 'Config/notifications'

export function getEditor () {
  let id = storage.get('id')
  let authtoken = storage.get('authtoken')
  let rId
  Receipt.getActive(id, authtoken)
    .then(res => {
      // console.log(res.data)
      let pr
      if (res.data.length === 0) {
        pr = Receipt.create(authtoken)
          .then(res => {
            // console.log(res.data)
            return res.data._id
          })
      } else pr = Promise.resolve(res.data[0]._id)

      pr.then(id => {
        rId = id
        // console.log(rId)
        return Entry.getForReceipt(id)
      }).then(res => {
        let checkout = 0
        // console.log(res)
        for (let i = 0; i < res.data.length; i++) {
          res.data[i].total = res.data[i].price * res.data[i].qty
          checkout += res.data[i].price * res.data[i].qty
        }
        let data = { entries: res.data, checkout, receiptId: rId, productCount: res.data.length }
        let html = editorTemplate(data)
        showView(html)
        // console.log(document.querySelectorAll('.row a'))
        document.querySelectorAll('.row a').forEach(el => {
          el.addEventListener('click', (e) => {
            // console.log(e.target.dataset)
            Entry.remove(e.target.dataset.id)
              .then(res => {
                showInfo('Entry removed')
                getEditor()
              })
          })
        })
        document.querySelector('#checkoutBtn').addEventListener('click', (e) => {
          e.preventDefault()
          // console.log(e.target)
          let total = Number(document.querySelector('#create-receipt-form > input[type="hidden"]:nth-child(8)').value)
          let productCount = Number(document.querySelector('#create-receipt-form > input[type="hidden"]:nth-child(7)').value)
          if (productCount === 0) {
            return showError('You need to have at least one entry in a receipt to checkout')
          }
          // console.log('checking out')
          Receipt.checkout(rId, productCount, total)
            .then(res => {
              showInfo('Receipt checked out.')
              getEditor()
            })
        })
        let inputs = document.querySelectorAll('#create-entry-form input')

        document.getElementById('addItemBtn').addEventListener('click', (e) => {
          e.preventDefault()
          let name = inputs[0].value
          let qty = Number(inputs[1].value)
          let price = Number(inputs[2].value)
          if (name.length === 0) {
            showError('Product Name must not be blank')
          } else if (Number.isNaN(price) || Number.isNaN(qty)) {
            showError('Price and qty must be valid numbers')
          } else {
            Entry.add(name, qty, price, rId).then(res => {
              showInfo('Entry added')
              getEditor()
            })
          }
        })

        inputs[1].addEventListener('input', subTotal)
        inputs[2].addEventListener('input', subTotal)

        function subTotal (e) {
          let qty = inputs[1].value
          let price = inputs[2].value
          let qty2 = Number(qty)
          let price2 = Number(price)
          let subtotal = (qty2 * price2)
          if (!Number.isNaN(subtotal) && qty.length > 0 && price.length > 0) {
            // console.log(document.querySelector('#create-entry-form .col'))
            document.querySelectorAll('#create-entry-form > div')[3].textContent = subtotal.toFixed(2) + ''
            let total = document.querySelector('#create-receipt-form > input[type="hidden"]:nth-child(8)')
            let totalDOM = document.querySelector('#create-receipt-form > div:nth-child(4)')
            totalDOM.textContent = (Number(total.value) + subtotal).toFixed(2)
          }
        }
      })
    })
    .catch(err => showError(err.message))
}
