import * as Entry from 'Models/Entry'
import detailsTemplate from 'Views/receiptDetails.handlebars'

import showView from 'Utilities/showView'

export function getDetails () {
  let id = window.location.hash.split('/')[1]
  Entry.getForReceipt(id)
    .then(res => {
      res.data.forEach(obj => {
        obj.subtotal = Number(obj.qty) * Number(obj.price)
      })

      let context = { entries: res.data }
      let html = detailsTemplate(context)
      showView(html)
    })
}
