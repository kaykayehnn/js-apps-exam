import * as Receipt from 'Models/Receipt'
import overviewTemplate from 'Views/overview.handlebars'

import showView from 'Utilities/showView'
import * as storage from 'Utilities/storage'

export function getOverview () {
  let id = storage.get('id')
  Receipt.getOverview(id)
    .then(res => {
      let total = 0
      res.data.forEach(obj => {
        let str = obj._kmd.ect
        total += obj.total
        obj.timestamp = `${str.slice(0, 10)} ${str.slice(11, 16)}`
      })

      let data = { receipts: res.data, total: total }
      let html = overviewTemplate(data)
      showView(html)
    })
}
