export function get (loggedIn) {
  if (loggedIn) {
    window.location.hash = '#receiptEditor'
  } else {
    window.location.hash = '#logIn'
  }
}
