import * as homeController from './homeController/'
import * as userController from './userController/'
import * as receiptController from './receiptController/'

export {
  homeController as home,
  userController as user,
  receiptController as receipt
}
