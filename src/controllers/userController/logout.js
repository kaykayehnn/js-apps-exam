import * as User from 'Models/User'

import * as storage from 'Utilities/storage'
import { showError, showInfo } from 'Config/notifications'

export function logOut () {
  User.logOut(storage.get('authtoken'))
    .then(res => {
      showInfo('Logout successful.')
      storage.clear()
      window.location.hash = '#'
    }).catch(err => showError(err.message))
}
