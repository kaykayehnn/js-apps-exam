import * as User from 'Models/User'
import loginTemplate from 'Views/authentication.handlebars'

import showView from 'Utilities/showView'
import * as storage from 'Utilities/storage'
import { showError, showInfo } from 'Config/notifications'

export function getLogin () {
  let context = { login: true }
  let html = loginTemplate(context)

  showView(html)

  document.getElementById('loginBtn').addEventListener('click', (e) => {
    e.preventDefault()
    let inputs = document.querySelectorAll('#login-form > input')
    let username = inputs[0].value
    let password = inputs[1].value
    if (username.length < 5) {
      showError('Username must not be shorter than 5 characters')
    } else if (password.length === 0) {
      showError('Password must not be blank')
    } else {
      User.logIn(username, password)
        .then(res => {
          console.dir(res.data, typeof res)
          storage.saveSession(res.data)
          showInfo('Login successful.')
          window.location.hash = '#receiptEditor'
        }).catch(err => showError(err.message))
    }
  })

  document.getElementById('registerBtn').addEventListener('click', (e) => {
    e.preventDefault()
    let inputs = document.querySelectorAll('#register-form > input')
    let username = inputs[0].value
    let password = inputs[1].value
    let repeat = inputs[2].value
    // console.log(username, password, repeat)
    if (username.length < 5) {
      showError('Username must not be shorter than 5 characters')
    } else if (password.length === 0 || password !== repeat) {
      showError('Passwords must not be blank and should match')
    } else {
      User.register(username, password)
        .then(res => {
          // console.log(res)
          showInfo('User registration successful')
          storage.saveSession(res.data)
          window.location.hash = '#receiptEditor'
        }).catch(err => showError(err.message))
    }
  })
}
