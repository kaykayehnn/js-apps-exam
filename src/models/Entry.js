import axios from '../utilities/requester'

export function getForReceipt (id) {
  return axios.get(`/appdata/${APP_KEY}/entries?query={"receiptId":"${id}"}`)
}
export function add (name, qty, price, receiptId) {
  return axios.post(`/appdata/${APP_KEY}/entries`, {name, qty, price, receiptId})
}
export function remove (id) {
  return axios.delete(`/appdata/${APP_KEY}/entries/${id}`)
}
