import axios from '../utilities/requester'

export function getActive (id, authtoken) {
  return axios.get(`/appdata/${APP_KEY}/receipts?query={"_acl.creator":"${id}","active":true}`)
}
export function create (authtoken) {
  return axios.post(`/appdata/${APP_KEY}/receipts`, {
    'active': true,
    'productCount': 0,
    'total': 0
  })
}
export function checkout (id, productCount, total) {
  return axios.get(`/appdata/${APP_KEY}/receipts/${id}`)
    .then(res => {
      let rec = res.data
      rec.active = false
      rec.productCount = productCount
      rec.total = total
      return axios.put(`/appdata/${APP_KEY}/receipts/${id}`, rec)
    })
}
export function getOverview (id) {
  return axios.get(`/appdata/${APP_KEY}/receipts?query={"_acl.creator":"${id}","active":false}`)
}
export function getReceipt (id) {
  return axios.get(`/appdata/${APP_KEY}/receipts/${id}`)
}
