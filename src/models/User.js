import axios from '../utilities/requester'

export function register (username, password) {
  let userData = {
    username,
    password
  }
  return axios.post(`/user/${APP_KEY}`, userData)
}

export function logIn (username, password) {
  let userData = {username, password}
  return axios.post(`/user/${APP_KEY}/login`, userData)
}

export function logOut (authtoken) {
  return axios.post(`/user/${APP_KEY}/_logout`)
}
