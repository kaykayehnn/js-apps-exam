import '../style/site.css'
import '../style/header.css'
import '../style/sections.css'

import renderNav from './nav'
import { showError } from './notifications'
import * as storage from 'Utilities/storage'
import * as controllers from 'Controllers/index'

const handlerMap = [
  {
    test: /^#?$/,
    handler: controllers.home.get
  },
  {
    test: '#logIn',
    handler: controllers.user.getLogin
  },
  {
    test: '#logout',
    handler: controllers.user.logOut
  },
  {
    test: '#receiptEditor',
    handler: controllers.receipt.getEditor
  },
  {
    test: '#overview',
    handler: controllers.receipt.getOverview
  },
  {
    test: /#details\/[\da-f]+/,
    handler: controllers.receipt.getDetails
  }
]

window.addEventListener('hashchange', router, false)

export default function router () {
  let hash = window.location.hash
  renderNav(storage.get('username'))
  let loggedIn = !!storage.get('id')
  if (!loggedIn) {
    return controllers.user.getLogin()
  }
  for (let i = 0; i < handlerMap.length; i++) {
    let obj = handlerMap[i]
    let test = obj.test
    if ((typeof test === 'string' && test === hash) || (test.test && test.test(hash))) {
      obj.handler(loggedIn)
      return
    }
  }
  showError('Invalid URL')
}
