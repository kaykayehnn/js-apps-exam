import navTemplate from 'Views/navbar.handlebars'

export default function renderNav (user) {
  let context = { user }
  let html = navTemplate(context)

  document.getElementById('profile').innerHTML = html
}
