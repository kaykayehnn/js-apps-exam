import '../style/notifications.css'

function toggleDiv (divId, show, message) {
  let div = typeof divId === 'string'
    ? document.getElementById(divId) // first argument can be selector
    : divId // or DOM node

  div.style.display = show ? 'block' : 'none'
  if (message) div.innerHTML = `<span>${message}</span>`
}

let timeout

document.addEventListener('click', (e) => {
  try {
    let p = e.target.parentElement
    // console.log(e.target.parentElement)
    // console.log(p.id)
    if (p.id === 'infoBox' || p.id === 'errorBox') {
      p.style.display = 'none'
      if (timeout) clearTimeout(timeout)
      timeout = null
    }
  } catch (e) {

  }
})

export function showInfo (message) {
  if (timeout) clearTimeout(timeout)
  hideOthers()
  toggleDiv('infoBox', true, message)
  timeout = setTimeout(() => {
    toggleDiv('infoBox', false)
    timeout = null
  }, 3000)
}

export function showError (message) {
  hideOthers()
  toggleDiv('errorBox', true, message)
}

let loadingShown = false

export function toggleLoading (message) {
  loadingShown = !loadingShown
  toggleDiv('loadingBox', loadingShown)
}

function hideOthers () {
  document.querySelectorAll('#notifications div').forEach(el => {
    el.style.display = 'none'
  })
}
