# Point of Sale

Solution for JS Apps Exam on 2018-04-15. Hosted on [Netlify].

### Models:
  - Users
  - Receipts
  - Entries

### Views:
  - Sign in / Register
  - Receipt Editor (Home page for logged in users)
  - Receipts Overview (My receipts)
  - Receipt Details

## Tech

Point of Sale uses a number of open source projects to work properly:

* [Webpack] - Asset bundler
* [Handlebars] - Templating engine
* [axios] - Promise based HTTP client

And most importantly no [jQuery]!

![alt text](https://media.giphy.com/media/wogOH1CsDqWMRUAsN4/giphy.gif "Chuck Norris Approves")
## Installation

Install the dependencies and devDependencies and start the server.

```sh
$ npm install
$ npm start
```

For production environments...

```sh
$ npm install
$ npm run build
```

License
----

MIT

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [Webpack]: <https://webpack.js.org>
   [Handlebars]: <http://handlebarsjs.com>
   [axios]: <https://github.com/axios/axios>
   [jQuery]: <https://github.com/jquery/jquery>
   [Netlify]: <https://pointofsale.netlify.com>
